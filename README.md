<p align="center"><img src="https://www.infostride.com/hs-fs/hubfs/Info_Stride_March2020_Theme/Image/logo.png" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Installation

### Step 1
You can install the package via git:

```shell
git clone git@gitlab.com:Shivrag/test-job-birthday-wish.git
```

#### Package install

Run the command : 

- The Laravel package will automatically register itself, so you can start using it immediately.

```shell
composer install
```

- The npm package will automatically register itself, so you can start using it immediately.

```shell
npm install && npm run prod
```

#### Key generate

Run the command : 

- The Laravel key generate immediately.

```shell
php artisan key:generate
```

### Step 2 - SetUp database

- Open .env file set APP_NAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD and SMTP details"
- Run the command : 

```shell
composer dump-autoload
```

```shell
php artisan migrate
```

### Step 3 - SetUp Cronjob for Birthday wish

- Run the command : 

```shell
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

## Usage

- Run the command: 

```shell
php artisan serve
```

## Contributing
- Infostride this project
- Clone to your repo
- Make your changes and run tests `composer test`
- Push and create Pull Request

## Screenshot

[![login.png](https://i.postimg.cc/Gmf2NCH7/login.png)](https://postimg.cc/qN84hWcy)[![registration.png](https://i.postimg.cc/SxRRDsNY/registration.png)](https://postimg.cc/CRywKw8w)
[![dashboard.png](https://i.postimg.cc/WbXtGrjP/dashboard.png)](https://postimg.cc/gnLYdxnM)![Login](https://i.postimg.cc/Gmf2NCH7/login.png)
[![welcome-mail.png](https://i.postimg.cc/Pq8xnRDt/welcome-mail.png)](https://postimg.cc/CZSYjJJt)
[![birthday-mail.png](https://i.postimg.cc/SQLnK3vR/birthday-mail.png)](https://postimg.cc/Z0Rb77Zz)

