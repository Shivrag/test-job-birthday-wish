<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Notifications\BirthdayWishNotification;

class BirthdayWish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To Wish birthday a day before';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $next_day =  explode('-', \Carbon\Carbon::tomorrow()->format('d-m'));
        $users = User::whereDay('dob', $next_day[0])->whereMonth('dob', $next_day[1])->get();
        foreach ($users as $user) {
            $user->notify(new BirthdayWishNotification($user));
        }
        $this->info('BirthdayWish:Cron Cummand Run successfully!');
    }
}
